Prep-Project (Get-Path "PortableLogAdapter")
Prep-Project (Get-Path "Adapters\NLogAdapter")
Prep-Project (Get-Path "Adapters\log4netAdapter")

Run-Command $env:TextTransformExe @((Get-Path "PortableLogAdapter\Types\LogLevel.tt"))
Run-Command $env:TextTransformExe @((Get-Path "PortableLogAdapter\Contracts\ILogger.Levels.Extensions.tt"))

Build (Get-Path "PortableLogAdapter") "PortableLogAdapter.csproj"
Build (Get-Path "Adapters\NLogAdapter") "NLogAdapter.csproj"
Build (Get-Path "Adapters\log4netAdapter") "log4netAdapter.csproj"

Build-Project (Get-Path "Test\Test.csproj")
Run-Tests (Get-Path "Test\bin\$env:Configuration\Acrotech.PortableLogAdapter.Test.dll")

Build-Project (Get-Path "Adapters\NLogAdapter.Test\NLogAdapter.Test.csproj")
Run-Tests (Get-Path "Adapters\NLogAdapter.Test\bin\$env:Configuration\Acrotech.PortableLogAdapter.NLogAdapter.Test.dll")

Build-Project (Get-Path "Adapters\log4netAdapter.Test\log4netAdapter.Test.csproj")
Run-Tests (Get-Path "Adapters\log4netAdapter.Test\bin\$env:Configuration\Acrotech.PortableLogAdapter.log4netAdapter.Test.dll")

# Until NuGet 3.0.0 is released (and deployed to MyGet and other build services), we need to use the patched NuGet stored locally.
# this is a workaround to packing PCL libraries with replacement tokens in the nuspec
$env:NuGetExe = (Get-Path ".nuget\NuGet.exe")
Log-Build "Updated NuGetExe to $env:NuGetExe"

Pack-Packagable (Get-Path "PortableLogAdapter\PortableLogAdapter.csproj")
Pack-Packagable (Get-Path "Adapters\NLogAdapter\NLogAdapter.csproj")
Pack-Packagable (Get-Path "Adapters\log4netAdapter\log4netAdapter.csproj")
