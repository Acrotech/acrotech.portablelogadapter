# README #

| Host  | develop | beta | master |
|-------|---------|------|--------|
| MyGet | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech-develop?identifier=eb731f1d-67a3-44b7-812f-264499f20262)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech-beta?identifier=5eceb06f-9690-4f47-a016-423425f3b658)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech?identifier=a6b98ae5-48e8-42c4-ab75-e6ff2dcac264)](https://www.myget.org/gallery/acrotech) |

## About ##

`Acrotech.PortableLogAdapter` is an abstract logging library that allows portable class libraries (PCL's) to access and perform logging operations without requiring a strong reference to the platform logging environment. Typically, logging systems are not built to run in a portable environment because portable classes lack the ability to log to many common targets (i.e., log file output). Using this portable system, the logging functionality is split, and through inversion of control, the platform logging system is called from the portable class(es).

This library was designed to specifically work very well with [NLog](http://nlog-project.org/) (this is my go-to logging system) but can easily be adapted to use any logging system. By using this library, you can easily use dependency injection or simple construction to produce loggers required by any class. The interface is fully scalable to any type of logging system and can be adjusted very easily using the T4 templates to produce new extension methods or logging levels. This library is built as a PCL to support most common deployments without any additional immediate dependencies (platform dependencies required for the specific logging system desired, or none if you're happy with `Debug.WriteLine` or something similar when using  the `DelegateLogManager`).

This library targets PCL **Profile 344** and is therefore a universally portable library.

## Instructions ##

0. This NuGet package should be added to any classes that will be calling any logging functions (basically all projects should reference the `PortableLogAdapter` package).
0. Create a portable log manager (`ILogManager` implementation) and distribute a reference across the entire application
    * The portable library comes with a very basic `ILogManager` implementation (the `DelegateLogManager`) which you can use to get started immediate without any further setup (see below for more details on how to use this log manager).
    * Install one of the available adapters (`NLogAdapter` or `log4netAdapter`) that are available on NuGet (see below for more details on how to use these log managers).
    * Either use **Dependency Injection**, **Construction**, or a **Static Accessor** to provide all of your classes with a reference to the `ILogManager` interface for your chosen log adapter.
0. Use the `ILogManager` to create a logger for a class (or by using a logger name).

## Log Managers ##

### `NullLogManager` ###

This is a developmental log manager that gives you immediate access to a strong reference, but does not perform any actual logging functions. This log manager is built into the portable class library, and therefore is available to any classes without additional dependencies. This logger returns a singleton `NullLogger` whenever a Logger is requested; the `NullLogger` has no logging code implemented. This log manager is access via the singleton accessor `NullLogger.Default`.

This log manager is useful if you want to stub in logging quickly and would like to deal with the actual logging output at a later time.

### `DelegateLogManager` ###

This is another developmental log manager, but one that could also be used as a very simple production log manager. This log manager uses a logging delegate function that is passed onto created `DelegateLogger` instances whenever a logger is requested.

This log manager is useful if you want to set up a very basic logging system that requires next to no setup time, and allows seamless replacement with a more complete logging system at a later time.

The easiest way to get started with this log manager is to create a new manager with a `Debug.WriteLine()` delegate. By default all loggers will be created at the `Trace` level, but you can adjust `DelegateLogManager.DefaultLevel` to decrease the verbosity (***NOTE***: this does not affect previously created loggers).

```csharp
// this would be placed somewhere central, App.xaml.cs for example
// we use DelegateLogManager here for simplicity, but this could be replaced with any other adapter
// at any time to seamlessly transition to a full logging system
public static readonly ILogManager LogManager =
  new DelegateLogManager((logger, message) => System.Diagnostics.Debug.WriteLine("[{0}]{1}", logger.Name, message), LogLevel.Info);

// use any of the GetLogger functions
ILogger logger1 = LogManager.GetLogger("name");
ILogger logger2 = LogManager.GetLogger(this);
ILogger logger3 = LogManager.GetLogger(GetType());
ILogger logger4 = LogManager.GetLogger<MyClass>();

// now just log using the rich API
logger1.Debug("this is a {0} log message", "Test");
logger2.Info(() => FetchMessageFromExpensiveFunction());
logger3.WarnException(new Exception(), "A wild exception has appeared");
logger4.Log(LogLevel.Trace, "this is a test");
```

### `NLogManager` ###

This log manager provides access to the [NLog](http://nlog-project.org/) logging system. You must install the corresponding NuGet package to your platform project (which will install the required `NLog` package as well). This log manager, like the others, is accessible via the singleton accessor `NLogManager.Default`.

***NOTE***: you must perform your own configuration and initialization of the `NLog` system prior to using this log manager. The recommended approach is to use an XML configuration [embedded within your app.confg file](https://github.com/nlog/nlog/wiki/Configuration-file#configuration-file-format).

### `Log4NetManager` ###

This log manager provides access to the (Apache) [log4net](http://logging.apache.org/log4net/) logging system. You must install the corresponding NuGet package to your platform project (which will install the required `log4net` package as well). This log manager, like the others, is accessible via the singleton accessor `Log4NetManager.Default`.

This log manager is not fully tested yet and will remain in *PreRelease* mode until full testing is completed.

***NOTE***: you must perform your own configuration and initialization of the `log4net` system prior to using this log manager.
