﻿using System;

namespace Acrotech.PortableLogAdapter.Loggers
{
    /// <summary>
    /// Simple logger that forwards all log calls to the provided delegate action
    /// </summary>
    public class DelegateLogger : ILogger
    {
        /// <summary>
        /// Creates a simple name and delegate based delegate logger
        /// </summary>
        /// <param name="name">Logger name</param>
        /// <param name="level">Log level</param>
        /// <param name="delegateAction">Logger delegate action</param>
        public DelegateLogger(string name, LogLevel level, Action<ILogger, string> delegateAction)
        {
            Name = name;
            LogLevel = level;
            DelegateAction = delegateAction;
        }

        /// <inheritdoc/>
        public string Name { get; private set; }

        private Action<ILogger, string> DelegateAction { get; set; }

        /// <inheritdoc/>
        public void Log(LogLevel level, string format, params object[] args)
        {
            if (DelegateAction != null && this.IsEnabled(level))
            {
                DelegateAction(this, "[{0,-5}] {1}".FormatWith(level, format.FormatWith(args)));
            }
        }

        /// <inheritdoc/>
        public void Log(LogLevel level, Func<string> messageCreator)
        {
            Log(level, messageCreator());
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            Log(level, "{0}: {1}".FormatWith(format.FormatWith(args), exception));
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            LogException(level, exception, messageCreator());
        }

        /// <inheritdoc/>
        public LogLevel LogLevel { get; set; }
    }
}
