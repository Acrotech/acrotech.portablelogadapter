﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableLogAdapter
{
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Format the <paramref name="format"/> with the provided <paramref name="args"/> only if 
        /// the format is non-null and non-empty, and there are <paramref name="args"/> to format with.
        /// </summary>
        /// <param name="format">String to Format</param>
        /// <param name="args">Format Args</param>
        /// <returns>The formatted string, or <paramref name="format"/> if there is nothing to format</returns>
        public static string FormatWith(this string format, params object[] args)
        {
            var message = format;

            if (string.IsNullOrEmpty(format) == false && args != null && args.Length > 0)
            {
                message = string.Format(format, args);
            }

            return message;
        }
    }
}
