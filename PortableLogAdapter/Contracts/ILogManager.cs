﻿namespace Acrotech.PortableLogAdapter
{
    /// <summary>
    /// Interface for a custom log manager
    /// </summary>
    public interface ILogManager
    {
        /// <summary>
        /// Produces a logger using the <paramref name="name"/> provided
        /// </summary>
        /// <param name="name">Name to supply the logger with</param>
        /// <returns>A logger</returns>
        ILogger GetLogger(string name);
    }
}
