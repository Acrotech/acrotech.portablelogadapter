# README #

#### MyGet Feed ####

| Package    | develop | beta | master |
|------------|---------|------|--------|
| `Acrotech.PortableLogAdapter` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |

#### NuGet Feed ####

| Package | beta | master | downloads |
|---------|------|--------|-----------|
| `Acrotech.PortableLogAdapter` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableLogAdapter) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableLogAdapter) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.PortableLogAdapter.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableLogAdapter) |

## About ##

This library is intended to provide [PortableLogAdapter framework](https://bitbucket.org/Acrotech/acrotech.portablelogadapter) support to portable classes. When used with an adapter, the portable classes can perform logging operations.

## Usage Notes ##

*None Yet*

## Changelog ##

#### 1.1.0 ####

* Moved to a single repository for the library and all adapters
* log4net adapter is now available from the NuGet repository
* Much improved testing
* Automated builds using MyGet
* `DebugLogManager` is now simply `DelegateLogManager` and no longer imposes any Debug related delegates
* Bug fix for `IsEnabled` extension method when the `ILogger` source is null
* Bug fix for the `DelegateLogger`, now handles the `LogLevel` properly

#### 1.0.1.0 ####

* Improving NuGet build process by using the developmentDependency attribute
* Minor NuGet metadata adjustments
* Updating FormatSafe to break out early from format attempts when there are no args to format with
* Removing build dependency on the Tangible T4 editor
* Cleaning up T4 template generated files by using spaces instead of tabs for verbatim text
* Updating PCL profile to 344 (maximum compatibility)

#### 1.0.0.7 ####

* Fixing namespace for VersionDetails (forgot to update it after updating the NuGet package)

#### 1.0.0.6 ####

* Updating Versioning to fix an issue with a global class name collision

#### 1.0.0.5 ####

* Fixing some NuGet build issues

#### 1.0.0.4 ####

* Using Acrotech.Versioning for versioning

#### 1.0.0.3 ####

* Adding XMLDOC comments (and including the XML file in the NuGet package)
* Updating some NuGet related files

#### 1.0.0.2 ####

* Eliminating the .Contracts namespace to simplify access to the interfaces and extension methods (sorry for breaking changes)
* Eliminating the .Types namespace to simplify access to the interfaces and extension methods (sorry for breaking changes)
* Code cleanup

#### 1.0.0.1 ####

* Improved unit tests
* A few more extension methods to simplify log creation

#### 1.0.0.0 ####

* Initial Release
