﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.PortableLogAdapter
{
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Returns all valid log levels that can be used to log a message
        /// </summary>
        /// <returns>All valid log levels</returns>
        /// <remarks>Returns all LogLevel enum values except Off</remarks>
        public static IEnumerable<LogLevel> GetAllLogLevels()
        {
            return 
#if PocketPC
                typeof(LogLevel).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static)
                    .Select(x => x.GetValue(null))
#else
                Enum.GetValues(typeof(LogLevel))
#endif
                    .Cast<LogLevel>()
                    .Where(x => x != LogLevel.Off);
        }

        /// <summary>
        /// Checks if <paramref name="source"/> level is enabled under <paramref name="level"/>
        /// </summary>
        /// <param name="source">LogLevel to check</param>
        /// <param name="level">LogLevel to check against</param>
        /// <returns>true if <paramref name="source"/> is enabled under <paramref name="level"/>, false otherwise</returns>
        public static bool IsEnabledFor(this LogLevel source, LogLevel level)
        {
            return source >= level;
        }
    }
}
