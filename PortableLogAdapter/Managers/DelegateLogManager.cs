﻿using System.Diagnostics;
using Acrotech.PortableLogAdapter.Loggers;
using System;

namespace Acrotech.PortableLogAdapter.Managers
{
    /// <summary>
    /// Delegate Log Manager creates a <see cref="Acrotech.PortableLogAdapter.Loggers.DelegateLogger"/> that writes to the configured logging delegate
    /// </summary>
    public class DelegateLogManager : ILogManager
    {
        /// <summary>
        /// Create a new delegate log manager
        /// </summary>
        /// <param name="delegateAction">Logging delegate</param>
        public DelegateLogManager(Action<ILogger, string> delegateAction) : this(delegateAction, LogLevel.Trace)
        {
        }

        /// <summary>
        /// Create a new delegate log manager
        /// </summary>
        /// <param name="delegateAction">Logging delegate</param>
        /// <param name="defaultLevel">Default logger level (defaults to Trace)</param>
        public DelegateLogManager(Action<ILogger, string> delegateAction, LogLevel defaultLevel)
        {
            if (delegateAction == null)
            {
                throw new ArgumentNullException("delegateAction", "The delegate cannot be null");
            }

            DelegateAction = delegateAction;
            DefaultLevel = defaultLevel;
        }

        private Action<ILogger, string> DelegateAction { get; set; }

        /// <summary>
        /// Logging level used when creating loggers
        /// </summary>
        public LogLevel DefaultLevel { get; set; }

        /// <inheritdoc/>
        public ILogger GetLogger(string name)
        {
            return new DelegateLogger(name, DefaultLevel, DelegateAction);
        }
    }
}
