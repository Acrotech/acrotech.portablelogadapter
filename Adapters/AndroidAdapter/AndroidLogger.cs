using Android.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Acrotech.PortableLogAdapter.AndroidAdapter
{
    public class AndroidLogger : ILogger
    {
        private static readonly LogPriority Suppress = (LogPriority)((int)LogPriority.Assert + 1);

        private static readonly Dictionary<LogPriority, LogLevel> AndroidToPortableMap = new Dictionary<LogPriority, LogLevel>()
        {
            { LogPriority.Verbose, LogLevel.Trace },
            { LogPriority.Debug, LogLevel.Debug },
            { LogPriority.Info, LogLevel.Info },
            { LogPriority.Warn, LogLevel.Warn },
            { LogPriority.Error, LogLevel.Error },
            { LogPriority.Assert, LogLevel.Fatal },
            { Suppress, LogLevel.Off },
        };

        private static readonly Dictionary<LogLevel, LogPriority> PortableToAndroidMap = new Dictionary<LogLevel, LogPriority>()
        {
            { LogLevel.Trace, LogPriority.Verbose },
            { LogLevel.Debug, LogPriority.Debug },
            { LogLevel.Info, LogPriority.Info },
            { LogLevel.Warn, LogPriority.Warn },
            { LogLevel.Error, LogPriority.Error },
            { LogLevel.Fatal, LogPriority.Assert },
            { LogLevel.Off, Suppress },
        };

        public AndroidLogger(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }

            Name = name;
        }

        private static LogPriority Convert(LogLevel level)
        {
            return PortableToAndroidMap[level];
        }

        private static LogLevel Convert(LogPriority level)
        {
            var result = LogLevel.Off;

            if (AndroidToPortableMap.TryGetValue(level, out result) == false)
            {
                result = GetNearestLogLevel(level);
            }

            return result;
        }

        private static LogLevel GetNearestLogLevel(LogPriority level)
        {
            var result = LogLevel.Off;

            foreach (var l in AndroidToPortableMap.Keys)
            {
                if ((int)level <= (int)l)
                {
                    result = AndroidToPortableMap[l];

                    break;
                }
            }

            return result;
        }

        private static LogPriority GetPriority(string name)
        {
            return AndroidToPortableMap.Keys
                .Cast<LogPriority?>()
                .FirstOrDefault(x => x != Suppress && Android.Util.Log.IsLoggable(name, x.Value)) ?? Suppress;
        }

        #region ILogger Members

        /// <inheritdoc/>
        public string Name { get; private set; }

        /// <inheritdoc/>
        public void Log(LogLevel level, string format, params object[] args)
        {
            Android.Util.Log.WriteLine(Convert(level), Name, format, args);
        }

        /// <inheritdoc/>
        public void Log(LogLevel level, Func<string> messageCreator)
        {
            var priority = Convert(level);

            if (Android.Util.Log.IsLoggable(Name, priority))
            {
                Android.Util.Log.WriteLine(priority, Name, messageCreator());
            }
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            LogException(level, exception, () => format.FormatWith(args));
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            var priority = Convert(level);

            if (Android.Util.Log.IsLoggable(Name, priority))
            {
                var throwable = Java.Lang.Throwable.FromException(exception);

                // there is no way to generically log an exception with a priority, 
                // so we must use a switch statement to make it happen here.
                switch (level)
                {
                    case LogLevel.Trace:
                        Android.Util.Log.Verbose(Name, throwable, messageCreator());
                        break;
                    case LogLevel.Debug:
                        Android.Util.Log.Debug(Name, throwable, messageCreator());
                        break;
                    case LogLevel.Info:
                        Android.Util.Log.Info(Name, throwable, messageCreator());
                        break;
                    case LogLevel.Warn:
                        Android.Util.Log.Warn(Name, throwable, messageCreator());
                        break;
                    case LogLevel.Error:
                        Android.Util.Log.Error(Name, throwable, messageCreator());
                        break;
                    case LogLevel.Fatal:
                        Android.Util.Log.Wtf(Name, throwable, messageCreator());
                        break;
                }
            }
        }

        /// <inheritdoc/>
        public LogLevel LogLevel { get { return Convert(GetPriority(Name)); } set { throw new InvalidOperationException("Android does not support changing logger levels at runtime"); } }

        #endregion
    }
}