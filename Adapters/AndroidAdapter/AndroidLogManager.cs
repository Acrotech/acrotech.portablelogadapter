namespace Acrotech.PortableLogAdapter.AndroidAdapter
{
    /// <summary>
    /// This class creates Android loggers
    /// </summary>
    public class AndroidLogManager : ILogManager
    {
        /// <summary>
        /// This is the default (and only) NLogManager instance
        /// </summary>
        public static readonly AndroidLogManager Default = new AndroidLogManager();

        private AndroidLogManager()
        {
        }

        /// <summary>
        /// Creates a new AndroidLogger with the provided <paramref name="name"/>
        /// </summary>
        /// <param name="name">Logger name</param>
        /// <returns>The AndroidLogger</returns>
        public ILogger GetLogger(string name)
        {
            return new AndroidLogger(name);
        }
    }
}
