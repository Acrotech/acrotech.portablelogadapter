﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using log4net.Layout;
using log4net.Appender;
using log4net.Repository.Hierarchy;

namespace Acrotech.PortableLogAdapter.log4netAdapter.Test
{
    [TestClass]
    public class Log4netLoggerTest
    {
        private static readonly log4net.Core.Level DefaultLevel = log4net.Core.Level.Debug;

        private MemoryAppender Configure(log4net.Core.Level level = null)
        {
            log4net.LogManager.ResetConfiguration();

            var repo = (Hierarchy)log4net.LogManager.GetRepository();
            var appender = new MemoryAppender();
            appender.ActivateOptions();

            repo.Root.AddAppender(appender);
            repo.Root.Level = level ?? DefaultLevel;
            repo.Configured = true;

            return appender;
        }

        [TestMethod]
        public void ConfigureTest()
        {
            var appender = Configure();

            Assert.IsNotNull(appender);

            var repo = (Hierarchy)log4net.LogManager.GetRepository();

            Assert.AreEqual(DefaultLevel, repo.Root.Level);
            Assert.AreEqual(1, repo.Root.Appenders.Count);
            Assert.AreSame(appender, repo.Root.Appenders[0]);
            Assert.IsTrue(repo.Configured);

            var logger = log4net.LogManager.GetLogger("test");
            logger.Debug("testing");

            var events = appender.GetEvents();
            Assert.IsNotNull(events);
            Assert.AreEqual(1, events.Length);
            Assert.AreEqual("testing", events[0].RenderedMessage);
        }

        [TestMethod]
        public void ConfigureLevelTest()
        {
            Configure(level: log4net.Core.Level.Error);

            var repo = (Hierarchy)log4net.LogManager.GetRepository();

            Assert.AreEqual(log4net.Core.Level.Error, repo.Root.Level);
        }

        [TestMethod]
        public void ResetTest()
        {
            Configure();

            log4net.LogManager.ResetConfiguration();

            var repo = (Hierarchy)log4net.LogManager.GetRepository();

            Assert.AreEqual(0, repo.Root.Appenders.Count);
            Assert.IsFalse(repo.Configured);
        }

        [TestMethod]
        public void NLogToPortableMapTest()
        {
            var pt = new PrivateType(typeof(Log4NetLogger));
            var map = (Dictionary<log4net.Core.Level, LogLevel>)pt.GetStaticField("Log4NetToPortableMap");

            Assert.AreEqual(LogLevel.Trace, map[log4net.Core.Level.Trace]);
            Assert.AreEqual(LogLevel.Debug, map[log4net.Core.Level.Debug]);
            Assert.AreEqual(LogLevel.Info, map[log4net.Core.Level.Info]);
            Assert.AreEqual(LogLevel.Warn, map[log4net.Core.Level.Warn]);
            Assert.AreEqual(LogLevel.Error, map[log4net.Core.Level.Error]);
            Assert.AreEqual(LogLevel.Fatal, map[log4net.Core.Level.Fatal]);
            Assert.AreEqual(LogLevel.Off, map[log4net.Core.Level.Off]);
        }

        [TestMethod]
        public void PortableToNlogMapTest()
        {
            var pt = new PrivateType(typeof(Log4NetLogger));
            var map = (Dictionary<LogLevel, log4net.Core.Level>)pt.GetStaticField("PortableToLog4NetMap");

            Assert.AreEqual(log4net.Core.Level.Trace, map[LogLevel.Trace]);
            Assert.AreEqual(log4net.Core.Level.Debug, map[LogLevel.Debug]);
            Assert.AreEqual(log4net.Core.Level.Info, map[LogLevel.Info]);
            Assert.AreEqual(log4net.Core.Level.Warn, map[LogLevel.Warn]);
            Assert.AreEqual(log4net.Core.Level.Error, map[LogLevel.Error]);
            Assert.AreEqual(log4net.Core.Level.Fatal, map[LogLevel.Fatal]);
            Assert.AreEqual(log4net.Core.Level.Off, map[LogLevel.Off]);
        }

        [TestMethod]
        public void ConstructorTest()
        {
            var l4nlogger = log4net.LogManager.GetLogger("test");
            var logger = new Log4NetLogger(l4nlogger);
            Assert.AreEqual(l4nlogger, logger.Logger);
            Assert.AreEqual("test", logger.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void ConstructorWithNullLoggerTest()
        {
            var logger = new Log4NetLogger(null);
        }

        [TestMethod]
        public void LogTest()
        {
            var appender = Configure(log4net.Core.Level.Trace);

            var l4nlogger = log4net.LogManager.GetLogger("test");
            var logger = new Log4NetLogger(l4nlogger);

            foreach (var level in Enum.GetValues(typeof(LogLevel)).Cast<LogLevel>().Where(x => x != LogLevel.Off))
            {
                logger.Log(level, "testing");

                var e = appender.GetEvents().Last();
                Assert.AreEqual(Log4NetLogger.Convert(level), e.Level);
                Assert.AreEqual("testing", e.RenderedMessage);
            }
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            var appender = Configure(log4net.Core.Level.Trace);

            var l4nlogger = log4net.LogManager.GetLogger("test");
            var logger = new Log4NetLogger(l4nlogger);

            var exception = new Exception();

            foreach (var level in Enum.GetValues(typeof(LogLevel)).Cast<LogLevel>().Where(x => x != LogLevel.Off))
            {
                logger.LogException(level, exception, "testing");

                var e = appender.GetEvents().Last();
                Assert.AreEqual(Log4NetLogger.Convert(level), e.Level);
                Assert.AreSame(exception, e.ExceptionObject);
                Assert.AreEqual("testing", e.RenderedMessage);
            }
        }

        [TestMethod]
        public void LogLevelTest()
        {
            Configure(log4net.Core.Level.Info);

            var l4nlogger = log4net.LogManager.GetLogger("test");
            var logger = new Log4NetLogger(l4nlogger);

            Assert.AreEqual(LogLevel.Info, logger.LogLevel);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void LogLevelSetterTest()
        {
            var l4nlogger = log4net.LogManager.GetLogger("test");
            var logger = new Log4NetLogger(l4nlogger);
            logger.LogLevel = LogLevel.Error;
        }

        [TestMethod]
        public void IsEnabledTest()
        {
            Configure(log4net.Core.Level.Info);

            var l4nlogger = log4net.LogManager.GetLogger("test");
            var logger = new Log4NetLogger(l4nlogger);
            var po = new PrivateObject(logger);

            Assert.IsFalse((bool)po.Invoke("IsEnabled", LogLevel.Debug));
            Assert.IsTrue((bool)po.Invoke("IsEnabled", LogLevel.Info));

            // log4net allows checking against the Off level
            Assert.IsTrue((bool)po.Invoke("IsEnabled", LogLevel.Off));
        }

        [TestMethod]
        public void ConvertToLog4NetLevelTest()
        {
            var pt = new PrivateType(typeof(Log4NetLogger));
            var map = (Dictionary<LogLevel, log4net.Core.Level>)pt.GetStaticField("PortableToLog4NetMap");

            foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
            {
                Assert.AreEqual(map[level], pt.InvokeStatic("Convert", level));
            }
        }

        [TestMethod]
        public void ConvertFromLog4NetLevelTest()
        {
            var pt = new PrivateType(typeof(Log4NetLogger));
            var map = (Dictionary<log4net.Core.Level, LogLevel>)pt.GetStaticField("Log4NetToPortableMap");

            Assert.AreEqual(map[log4net.Core.Level.Trace], pt.InvokeStatic("Convert", log4net.Core.Level.Trace));
            Assert.AreEqual(map[log4net.Core.Level.Debug], pt.InvokeStatic("Convert", log4net.Core.Level.Debug));
            Assert.AreEqual(map[log4net.Core.Level.Info], pt.InvokeStatic("Convert", log4net.Core.Level.Info));
            Assert.AreEqual(map[log4net.Core.Level.Warn], pt.InvokeStatic("Convert", log4net.Core.Level.Warn));
            Assert.AreEqual(map[log4net.Core.Level.Error], pt.InvokeStatic("Convert", log4net.Core.Level.Error));
            Assert.AreEqual(map[log4net.Core.Level.Fatal], pt.InvokeStatic("Convert", log4net.Core.Level.Fatal));
            Assert.AreEqual(map[log4net.Core.Level.Off], pt.InvokeStatic("Convert", log4net.Core.Level.Off));

            // un-mapped level
            Assert.AreEqual(LogLevel.Trace, pt.InvokeStatic("Convert", log4net.Core.Level.Finest));
        }

        [TestMethod]
        public void GetNearestLogLevelTest()
        {
            var pt = new PrivateType(typeof(Log4NetLogger));

            Assert.AreEqual(LogLevel.Trace, pt.InvokeStatic("GetNearestLogLevel", log4net.Core.Level.Trace));
            Assert.AreEqual(LogLevel.Debug, pt.InvokeStatic("GetNearestLogLevel", log4net.Core.Level.Debug));
            Assert.AreEqual(LogLevel.Info, pt.InvokeStatic("GetNearestLogLevel", log4net.Core.Level.Info));
            Assert.AreEqual(LogLevel.Warn, pt.InvokeStatic("GetNearestLogLevel", log4net.Core.Level.Warn));
            Assert.AreEqual(LogLevel.Error, pt.InvokeStatic("GetNearestLogLevel", log4net.Core.Level.Error));
            Assert.AreEqual(LogLevel.Fatal, pt.InvokeStatic("GetNearestLogLevel", log4net.Core.Level.Fatal));
            Assert.AreEqual(LogLevel.Off, pt.InvokeStatic("GetNearestLogLevel", log4net.Core.Level.Off));
        }

        [TestMethod]
        public void GetLog4NetLevelTest()
        {
            Configure(log4net.Core.Level.Error);

            var pt = new PrivateType(typeof(Log4NetLogger));

            var logger = log4net.LogManager.GetLogger("test");

            Assert.AreEqual(log4net.Core.Level.Error, pt.InvokeStatic("GetLog4NetLevel", logger));
        }
    }
}
