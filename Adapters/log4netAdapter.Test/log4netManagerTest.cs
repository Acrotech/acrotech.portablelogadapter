﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter.log4netAdapter;
using System.Diagnostics.CodeAnalysis;

namespace Acrotech.PortableLogAdapter.log4netAdapter.Test
{
    [TestClass]
    public class Log4netManagerTest
    {
        [TestMethod]
        public void DefaultManagerTest()
        {
            Assert.IsNotNull(Log4NetManager.Default);
        }

        [TestMethod]
        public void GetLoggerTest()
        {
            var logger = Log4NetManager.Default.GetLogger(string.Empty);
            Assert.IsNotNull(logger);
            Assert.AreEqual(string.Empty, logger.Name);
            // ensure no exceptions occur
            logger.Trace("test");

            logger = Log4NetManager.Default.GetLogger("test");
            Assert.IsNotNull(logger);
            Assert.AreEqual("test", logger.Name);
            // ensure no exceptions occur
            logger.Trace("test");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "log4net Doesn't Permit Null Logger Names")]
        [ExcludeFromCodeCoverageAttribute]
        public void GetLoggerNullNameTest()
        {
            Log4NetManager.Default.GetLogger(null);
        }
    }
}
