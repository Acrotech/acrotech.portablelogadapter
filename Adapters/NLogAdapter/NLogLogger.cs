﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Acrotech.PortableLogAdapter.NLogAdapter
{
    /// <summary>
    /// This class wraps logging to an NLog logger
    /// </summary>
    public class NLogLogger : ILogger
    {
        private static readonly Dictionary<NLog.LogLevel, LogLevel> NLogToPortableMap = new Dictionary<NLog.LogLevel, LogLevel>()
        {
            { NLog.LogLevel.Trace, LogLevel.Trace },
            { NLog.LogLevel.Debug, LogLevel.Debug },
            { NLog.LogLevel.Info, LogLevel.Info },
            { NLog.LogLevel.Warn, LogLevel.Warn },
            { NLog.LogLevel.Error, LogLevel.Error },
            { NLog.LogLevel.Fatal, LogLevel.Fatal },
            { NLog.LogLevel.Off, LogLevel.Off },
        };
        
        private static readonly Dictionary<LogLevel, NLog.LogLevel> PortableToNLogMap = new Dictionary<LogLevel, NLog.LogLevel>()
        {
            { LogLevel.Trace, NLog.LogLevel.Trace },
            { LogLevel.Debug, NLog.LogLevel.Debug },
            { LogLevel.Info, NLog.LogLevel.Info },
            { LogLevel.Warn, NLog.LogLevel.Warn },
            { LogLevel.Error, NLog.LogLevel.Error },
            { LogLevel.Fatal, NLog.LogLevel.Fatal },
            { LogLevel.Off, NLog.LogLevel.Off },
        };

        /// <summary>
        /// Creates a new NLog logger wrapper
        /// </summary>
        /// <param name="logger">Base NLog logger to funnel logging to</param>
        public NLogLogger(NLog.Logger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            Logger = logger;
        }

        /// <summary>
        /// Base NLog Logger associated with this NLogLogger
        /// </summary>
        public NLog.Logger Logger { get; private set; }

        private static NLog.LogLevel Convert(LogLevel level)
        {
            return PortableToNLogMap[level];
        }

        private static LogLevel Convert(NLog.LogLevel level)
        {
            var result = LogLevel.Off;

            if (NLogToPortableMap.TryGetValue(level, out result) == false)
            {
                result = GetNearestLogLevel(level);
            }

            return result;
        }

        private static LogLevel GetNearestLogLevel(NLog.LogLevel level)
        {
            var result = LogLevel.Off;

            foreach (var l in NLogToPortableMap.Keys)
            {
                if (level.Ordinal <= l.Ordinal)
                {
                    result = NLogToPortableMap[l];

                    break;
                }
            }

            return result;
        }

        private static NLog.LogLevel GetNLogLevel(NLog.Logger logger)
        {
            return NLogToPortableMap.Keys.FirstOrDefault(x => x != NLog.LogLevel.Off && logger.IsEnabled(x)) ?? NLog.LogLevel.Off;
        }

        #region ILogger Members

        /// <inheritdoc/>
        public string Name { get { return Logger.Name; } }

        /// <inheritdoc/>
        public void Log(LogLevel level, string format, params object[] args)
        {
            Logger.Log(Convert(level), format, args);
        }

        /// <inheritdoc/>
        public void Log(LogLevel level, Func<string> messageCreator)
        {
            Logger.Log(Convert(level), new NLog.LogMessageGenerator(messageCreator));
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            Logger.Log(Convert(level), format.FormatWith(args), exception);
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            Logger.Log(Convert(level), messageCreator(), exception);
        }

        /// <inheritdoc/>
        public LogLevel LogLevel { get { return Convert(GetNLogLevel(Logger)); } set { throw new InvalidOperationException("NLog does not support changing logger levels at runtime"); } }

        #endregion
    }
}
