# README #

#### MyGet Feed ####

| Package    | develop | beta | master |
|------------|---------|------|--------|
| `Acrotech.PortableLogAdapter.NLogAdapter` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |

#### NuGet Feed ####

| Package | beta | master | downloads |
|---------|------|--------|-----------|
| `Acrotech.PortableLogAdapter.NLogAdapter` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableLogAdapter.NLogAdapter) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableLogAdapter.NLogAdapter) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.PortableLogAdapter.NLogAdapter.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableLogAdapter.NLogAdapter) |

## About ##

This library is intended to provide NLog support for platform targets that use the [PortableLogAdapter framework](https://bitbucket.org/Acrotech/acrotech.portablelogadapter).

## Usage Notes ##

*None Yet*

## Changelog ##

#### 1.1.0 ####

* Bug fixes for log level conversions (for invalid levels)
* Bug fix for null `NLog.Logger` initialization (now throws an exception)
* `LogLevel` conversion functions are now private

#### 1.0.0.0 ####

* Initial Release
