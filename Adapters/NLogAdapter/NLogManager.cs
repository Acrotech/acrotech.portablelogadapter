﻿namespace Acrotech.PortableLogAdapter.NLogAdapter
{
    /// <summary>
    /// This class creates NLog loggers
    /// </summary>
    public class NLogManager : ILogManager
    {
        /// <summary>
        /// This is the default (and only) NLogManager instance
        /// </summary>
        public static readonly NLogManager Default = new NLogManager();

        private NLogManager()
        {
        }

        /// <summary>
        /// Creates a new NLogLogger with the provided <paramref name="name"/>
        /// </summary>
        /// <param name="name">Logger name</param>
        /// <returns>The NLogLogger</returns>
        public ILogger GetLogger(string name)
        {
            return new NLogLogger(NLog.LogManager.GetLogger(name));
        }
    }
}
