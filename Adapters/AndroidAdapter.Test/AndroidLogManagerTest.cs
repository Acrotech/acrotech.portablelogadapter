﻿using System;
using NUnit.Framework;

namespace Acrotech.PortableLogAdapter.AndroidAdapter.Test
{
    [TestFixture]
    public class AndroidLogManagerTest
    {
        [Test]
        public void DefaultManagerTest()
        {
            Assert.IsNotNull(AndroidLogManager.Default);
        }

        [Test]
        public void GetLoggerNonNullNameTest()
        {
            var logger = AndroidLogManager.Default.GetLogger(string.Empty);
            Assert.IsNotNull(logger);
            Assert.AreEqual(string.Empty, logger.Name);
            // ensure no exceptions occur
            logger.Trace("test");

            logger = AndroidLogManager.Default.GetLogger("test");
            Assert.IsNotNull(logger);
            Assert.AreEqual("test", logger.Name);
            // ensure no exceptions occur
            logger.Trace("test");
        }

        [Test]
        public void GetLoggerNullNameTest()
        {
            Assert.Catch<ArgumentNullException>(() => AndroidLogManager.Default.GetLogger(null));
        }
    }
}