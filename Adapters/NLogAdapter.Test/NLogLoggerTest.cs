﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter.NLogAdapter;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using NLog.Config;
using NLog.Targets;
using NLog.Layouts;

namespace Acrotech.PortableLogAdapter.NLogAdapter.Test
{
    [TestClass]
    public class NLogLoggerTest
    {
        private const string DefaultDebugTargetLayout = "[${level}] ${message}${onexception:\\: ${exception:format=tostring}}";

        private DebugTarget CreateDebugTarget(string layout = DefaultDebugTargetLayout)
        {
            return new DebugTarget() { Layout = layout };
        }

        private static DebugTarget GetDebugTarget(LoggingConfiguration config = null)
        {
            config = config ?? NLog.LogManager.Configuration;

            return (DebugTarget)config.ConfiguredNamedTargets[0];
        }

        private LoggingConfiguration CreateConfig(NLog.LogLevel minLevel = null, Target target = null, string pattern = "*")
        {
            var config = new LoggingConfiguration();

            minLevel = minLevel ?? NLog.LogLevel.Trace;
            target = target ?? CreateDebugTarget();

            config.AddTarget("UnitTest", target);
            config.LoggingRules.Add(new LoggingRule(pattern, minLevel, target));

            return config;
        }

        [TestMethod]
        public void CreateDebugTargetTest()
        {
            var target = CreateDebugTarget();
            Assert.IsNotNull(target);
            Assert.AreEqual(0, target.Counter);
            Assert.AreEqual(string.Empty, target.LastMessage);
            Assert.AreEqual(DefaultDebugTargetLayout, ((SimpleLayout)target.Layout).Text);

            target = CreateDebugTarget("${message}");
            Assert.IsNotNull(target);
            Assert.AreEqual(0, target.Counter);
            Assert.AreEqual(string.Empty, target.LastMessage);
            Assert.AreEqual("${message}", ((SimpleLayout)target.Layout).Text);
        }

        [TestMethod]
        public void GetDebugTargetTest()
        {
            NLog.LogManager.Configuration = CreateConfig();

            Assert.IsNotNull(GetDebugTarget());
            Assert.IsNotNull(GetDebugTarget(null));
            Assert.IsNotNull(GetDebugTarget(CreateConfig()));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [ExcludeFromCodeCoverage]
        public void GetDebugTargetForTargetlessConfigTest()
        {
            GetDebugTarget(new LoggingConfiguration());
        }

        [TestMethod]
        public void CreateConfigTest()
        {
            var config = CreateConfig();
            Assert.IsNotNull(config);
            Assert.AreEqual(1, config.ConfiguredNamedTargets.Count);
            Assert.AreEqual(1, config.LoggingRules.Count);
            Assert.AreEqual("*", config.LoggingRules[0].LoggerNamePattern);
            Assert.IsTrue(config.LoggingRules[0].IsLoggingEnabledForLevel(NLog.LogLevel.Trace));

            var target = CreateDebugTarget("${message}");
            config = CreateConfig(NLog.LogLevel.Info, target, "testing");
            Assert.IsNotNull(config);
            Assert.AreEqual(1, config.ConfiguredNamedTargets.Count);
            Assert.AreEqual(target, config.ConfiguredNamedTargets[0]);
            Assert.AreEqual(1, config.LoggingRules.Count);
            Assert.AreEqual("testing", config.LoggingRules[0].LoggerNamePattern);
            Assert.IsFalse(config.LoggingRules[0].IsLoggingEnabledForLevel(NLog.LogLevel.Trace));
            Assert.IsTrue(config.LoggingRules[0].IsLoggingEnabledForLevel(NLog.LogLevel.Info));
        }

        [TestMethod]
        public void NLogToPortableMapTest()
        {
            var pt = new PrivateType(typeof(NLogLogger));
            var map = (Dictionary<NLog.LogLevel, LogLevel>)pt.GetStaticField("NLogToPortableMap");

            Assert.AreEqual(LogLevel.Trace, map[NLog.LogLevel.Trace]);
            Assert.AreEqual(LogLevel.Debug, map[NLog.LogLevel.Debug]);
            Assert.AreEqual(LogLevel.Info, map[NLog.LogLevel.Info]);
            Assert.AreEqual(LogLevel.Warn, map[NLog.LogLevel.Warn]);
            Assert.AreEqual(LogLevel.Error, map[NLog.LogLevel.Error]);
            Assert.AreEqual(LogLevel.Fatal, map[NLog.LogLevel.Fatal]);
            Assert.AreEqual(LogLevel.Off, map[NLog.LogLevel.Off]);
        }

        [TestMethod]
        public void PortableToNlogMapTest()
        {
            var pt = new PrivateType(typeof(NLogLogger));
            var map = (Dictionary<LogLevel, NLog.LogLevel>)pt.GetStaticField("PortableToNLogMap");

            Assert.AreEqual(NLog.LogLevel.Trace, map[LogLevel.Trace]);
            Assert.AreEqual(NLog.LogLevel.Debug, map[LogLevel.Debug]);
            Assert.AreEqual(NLog.LogLevel.Info, map[LogLevel.Info]);
            Assert.AreEqual(NLog.LogLevel.Warn, map[LogLevel.Warn]);
            Assert.AreEqual(NLog.LogLevel.Error, map[LogLevel.Error]);
            Assert.AreEqual(NLog.LogLevel.Fatal, map[LogLevel.Fatal]);
            Assert.AreEqual(NLog.LogLevel.Off, map[LogLevel.Off]);
        }

        [TestMethod]
        public void ConstructorTest()
        {
            var nlogger = NLog.LogManager.GetLogger("test");
            var logger = new NLogLogger(nlogger);
            Assert.AreEqual(nlogger, logger.Logger);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void ConstructorWithNullLoggerTest()
        {
            var logger = new NLogLogger(null);
        }

        [TestMethod]
        public void ConvertToNLogLevelTest()
        {
            var pt = new PrivateType(typeof(NLogLogger));
            var map = (Dictionary<LogLevel, NLog.LogLevel>)pt.GetStaticField("PortableToNLogMap");

            foreach (LogLevel level in Enum.GetValues(typeof(LogLevel)))
            {
                Assert.AreEqual(map[level], pt.InvokeStatic("Convert", level));
            }
        }

        [TestMethod]
        public void ConvertFromNLogLevelTest()
        {
            var pt = new PrivateType(typeof(NLogLogger));
            var map = (Dictionary<NLog.LogLevel, LogLevel>)pt.GetStaticField("NLogToPortableMap");

            Assert.AreEqual(map[NLog.LogLevel.Trace], pt.InvokeStatic("Convert", NLog.LogLevel.Trace));
            Assert.AreEqual(map[NLog.LogLevel.Debug], pt.InvokeStatic("Convert", NLog.LogLevel.Debug));
            Assert.AreEqual(map[NLog.LogLevel.Info], pt.InvokeStatic("Convert", NLog.LogLevel.Info));
            Assert.AreEqual(map[NLog.LogLevel.Warn], pt.InvokeStatic("Convert", NLog.LogLevel.Warn));
            Assert.AreEqual(map[NLog.LogLevel.Error], pt.InvokeStatic("Convert", NLog.LogLevel.Error));
            Assert.AreEqual(map[NLog.LogLevel.Fatal], pt.InvokeStatic("Convert", NLog.LogLevel.Fatal));
            Assert.AreEqual(map[NLog.LogLevel.Off], pt.InvokeStatic("Convert", NLog.LogLevel.Off));
        }

        [TestMethod]
        public void ConvertFromInvalidNLogLevelTest()
        {
            var pt = new PrivateType(typeof(NLogLogger));

            // create a fake (invalid) NLog level
            var po = new PrivateObject(typeof(NLog.LogLevel), "Invalid", NLog.LogLevel.Off.Ordinal + 1);
            var nlevel = (NLog.LogLevel)po.Target;

            // because this fake NLog level is higher than Off, it should round down to Off
            Assert.AreEqual(LogLevel.Off, pt.InvokeStatic("Convert", nlevel));

            po.SetField("ordinal", -1);

            // because this fake NLog level is lower than Trace, it should round up to Trace
            Assert.AreEqual(LogLevel.Trace, pt.InvokeStatic("Convert", nlevel));
        }

        [TestMethod]
        public void GetNearestLogLevelTest()
        {
            var pt = new PrivateType(typeof(NLogLogger));

            Assert.AreEqual(LogLevel.Trace, pt.InvokeStatic("GetNearestLogLevel", NLog.LogLevel.Trace));
            Assert.AreEqual(LogLevel.Debug, pt.InvokeStatic("GetNearestLogLevel", NLog.LogLevel.Debug));
            Assert.AreEqual(LogLevel.Info, pt.InvokeStatic("GetNearestLogLevel", NLog.LogLevel.Info));
            Assert.AreEqual(LogLevel.Warn, pt.InvokeStatic("GetNearestLogLevel", NLog.LogLevel.Warn));
            Assert.AreEqual(LogLevel.Error, pt.InvokeStatic("GetNearestLogLevel", NLog.LogLevel.Error));
            Assert.AreEqual(LogLevel.Fatal, pt.InvokeStatic("GetNearestLogLevel", NLog.LogLevel.Fatal));
            Assert.AreEqual(LogLevel.Off, pt.InvokeStatic("GetNearestLogLevel", NLog.LogLevel.Off));
        }

        [TestMethod]
        public void GetNLogLevelTest()
        {
            var pt = new PrivateType(typeof(NLogLogger));

            var levels = new[]
            {
                NLog.LogLevel.Trace,
                NLog.LogLevel.Debug,
                NLog.LogLevel.Info,
                NLog.LogLevel.Warn,
                NLog.LogLevel.Error,
                NLog.LogLevel.Fatal,
                NLog.LogLevel.Off,
            };

            foreach (var level in levels)
            {
                NLog.LogManager.Configuration = CreateConfig(level);
                var logger = NLog.LogManager.GetLogger(string.Empty);
                Assert.AreEqual(level, pt.InvokeStatic("GetNLogLevel", logger));
            }
        }

        [TestMethod]
        public void NameTest()
        {
            var nlogger = NLog.LogManager.GetLogger(string.Empty);
            var logger = new NLogLogger(nlogger);
            Assert.AreEqual(nlogger.Name, logger.Name);

            nlogger = NLog.LogManager.GetLogger("test");
            logger = new NLogLogger(nlogger);
            Assert.AreEqual(nlogger.Name, logger.Name);
        }

        [TestMethod]
        public void LogTest()
        {
            NLog.LogManager.Configuration = CreateConfig();
            var target = GetDebugTarget();
            var nlogger = NLog.LogManager.GetLogger(string.Empty);
            var logger = new NLogLogger(nlogger);
            
            logger.Trace("test1");
            Assert.AreEqual("[Trace] test1", target.LastMessage);

            logger.Trace(() => "test2");
            Assert.AreEqual("[Trace] test2", target.LastMessage);
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            NLog.LogManager.Configuration = CreateConfig();
            var target = GetDebugTarget();
            var nlogger = NLog.LogManager.GetLogger(string.Empty);
            var logger = new NLogLogger(nlogger);
            var exception = new Exception("testing");

            logger.TraceException(exception, "test1");
            Assert.AreEqual("[Trace] test1: System.Exception: testing", target.LastMessage);

            logger.TraceException(exception, () => "test2");
            Assert.AreEqual("[Trace] test2: System.Exception: testing", target.LastMessage);
        }

        [TestMethod]
        public void LogLevelTest()
        {
            NLog.LogManager.Configuration = CreateConfig(NLog.LogLevel.Trace);
            var nlogger = NLog.LogManager.GetLogger(string.Empty);
            var logger = new NLogLogger(nlogger);
            Assert.AreEqual(LogLevel.Trace, logger.LogLevel);

            NLog.LogManager.Configuration = CreateConfig(NLog.LogLevel.Debug);
            nlogger = NLog.LogManager.GetLogger(string.Empty);
            logger = new NLogLogger(nlogger);
            Assert.AreEqual(LogLevel.Debug, logger.LogLevel);

            NLog.LogManager.Configuration = CreateConfig(NLog.LogLevel.Info);
            nlogger = NLog.LogManager.GetLogger(string.Empty);
            logger = new NLogLogger(nlogger);
            Assert.AreEqual(LogLevel.Info, logger.LogLevel);

            NLog.LogManager.Configuration = CreateConfig(NLog.LogLevel.Warn);
            nlogger = NLog.LogManager.GetLogger(string.Empty);
            logger = new NLogLogger(nlogger);
            Assert.AreEqual(LogLevel.Warn, logger.LogLevel);

            NLog.LogManager.Configuration = CreateConfig(NLog.LogLevel.Error);
            nlogger = NLog.LogManager.GetLogger(string.Empty);
            logger = new NLogLogger(nlogger);
            Assert.AreEqual(LogLevel.Error, logger.LogLevel);

            NLog.LogManager.Configuration = CreateConfig(NLog.LogLevel.Fatal);
            nlogger = NLog.LogManager.GetLogger(string.Empty);
            logger = new NLogLogger(nlogger);
            Assert.AreEqual(LogLevel.Fatal, logger.LogLevel);

            NLog.LogManager.Configuration = CreateConfig(NLog.LogLevel.Off);
            nlogger = NLog.LogManager.GetLogger(string.Empty);
            logger = new NLogLogger(nlogger);
            Assert.AreEqual(LogLevel.Off, logger.LogLevel);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void LogLevelSetterTest()
        {
            var nlogger = NLog.LogManager.GetLogger(string.Empty);
            var logger = new NLogLogger(nlogger);
            logger.LogLevel = LogLevel.Info;
        }
    }
}
