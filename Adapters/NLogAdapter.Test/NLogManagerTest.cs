﻿using Acrotech.PortableLogAdapter.NLogAdapter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Acrotech.PortableLogAdapter.NLogAdapter.Test
{
    [TestClass]
    public class NLogManagerTest
    {
        [TestMethod]
        public void DefaultManagerTest()
        {
            Assert.IsNotNull(NLogManager.Default);
        }

        [TestMethod]
        public void GetLoggerNonNullNameTest()
        {
            var logger = NLogManager.Default.GetLogger(string.Empty);
            Assert.IsNotNull(logger);
            Assert.AreEqual(string.Empty, logger.Name);
            // ensure no exceptions occur
            logger.Trace("test");

            logger = NLogManager.Default.GetLogger("test");
            Assert.IsNotNull(logger);
            Assert.AreEqual("test", logger.Name);
            // ensure no exceptions occur
            logger.Trace("test");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException), "NLog Doesn't Permit Null Logger Names")]
        [ExcludeFromCodeCoverageAttribute]
        public void GetLoggerNullNameTest()
        {
            NLogManager.Default.GetLogger(null);
        }
    }
}
