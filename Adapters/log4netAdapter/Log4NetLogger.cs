﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Acrotech.PortableLogAdapter.log4netAdapter
{
    /// <summary>
    /// This class wraps logging to a log4net logger
    /// </summary>
    public class Log4NetLogger : ILogger
    {
        private static readonly Dictionary<log4net.Core.Level, LogLevel> Log4NetToPortableMap = new Dictionary<log4net.Core.Level,LogLevel>()
        {
            { log4net.Core.Level.Trace, LogLevel.Trace },
            { log4net.Core.Level.Debug, LogLevel.Debug },
            { log4net.Core.Level.Info, LogLevel.Info },
            { log4net.Core.Level.Warn, LogLevel.Warn },
            { log4net.Core.Level.Error, LogLevel.Error },
            { log4net.Core.Level.Fatal, LogLevel.Fatal },
            { log4net.Core.Level.Off, LogLevel.Off },
        };

        private static readonly Dictionary<LogLevel, log4net.Core.Level> PortableToLog4NetMap = new Dictionary<LogLevel, log4net.Core.Level>()
        {
            { LogLevel.Trace, log4net.Core.Level.Trace },
            { LogLevel.Debug, log4net.Core.Level.Debug },
            { LogLevel.Info, log4net.Core.Level.Info },
            { LogLevel.Warn, log4net.Core.Level.Warn },
            { LogLevel.Error, log4net.Core.Level.Error },
            { LogLevel.Fatal, log4net.Core.Level.Fatal },
            { LogLevel.Off, log4net.Core.Level.Off },
        };

        /// <summary>
        /// Creates a new log4net logger wrapper
        /// </summary>
        /// <param name="logger">Base log4net (ILog) logger to funnel logging to</param>
        public Log4NetLogger(ILog logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            Logger = logger;
        }

        /// <summary>
        /// Base log4net (ILog) Logger associated with this Log4NetLogger
        /// </summary>
        public ILog Logger { get; private set; }

        /// <inheritdoc/>
        public string Name { get { return Logger.Logger.Name; } }

        /// <inheritdoc/>
        public void Log(LogLevel level, string format, params object[] args)
        {
            Log(level, () => format.FormatWith(args));
        }

        /// <inheritdoc/>
        public void Log(LogLevel level, Func<string> messageCreator)
        {
            if (IsEnabled(level))
            {
                switch (level)
                {
                    case LogLevel.Fatal:
                        Logger.Fatal(messageCreator());
                        break;
                    case LogLevel.Error:
                        Logger.Error(messageCreator());
                        break;
                    case LogLevel.Warn:
                        Logger.Warn(messageCreator());
                        break;
                    case LogLevel.Info:
                        Logger.Info(messageCreator());
                        break;
                    case LogLevel.Debug:
                        Logger.Debug(messageCreator());
                        break;
                    case LogLevel.Trace:
                        // ILog doesn't expose Trace so we have to manually call it
                        Logger.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, log4net.Core.Level.Trace, messageCreator(), null);
                        break;
                }
            }
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            LogException(level, exception, () => format.FormatWith(args));
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            if (IsEnabled(level))
            {
                switch (level)
                {
                    case LogLevel.Fatal:
                        Logger.Fatal(messageCreator(), exception);
                        break;
                    case LogLevel.Error:
                        Logger.Error(messageCreator(), exception);
                        break;
                    case LogLevel.Warn:
                        Logger.Warn(messageCreator(), exception);
                        break;
                    case LogLevel.Info:
                        Logger.Info(messageCreator(), exception);
                        break;
                    case LogLevel.Debug:
                        Logger.Debug(messageCreator(), exception);
                        break;
                    case LogLevel.Trace:
                        // ILog doesn't expose Trace so we have to manually call it
                        Logger.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, log4net.Core.Level.Trace, messageCreator(), exception);
                        break;
                }
            }
        }

        /// <inheritdoc/>
        public LogLevel LogLevel { get { return Convert(GetLog4NetLevel(Logger)); } set { throw new InvalidOperationException("Log4Net does not support changing logger levels at runtime"); } }

        private bool IsEnabled(LogLevel level)
        {
            return Logger.Logger.IsEnabledFor(Convert(level));
        }

        /// <summary>
        /// Converts a PortableLogAdapter level to a log4net level
        /// </summary>
        /// <param name="level">PortableLogAdapter level to convert</param>
        /// <returns>Equivalent log4net level</returns>
        public static log4net.Core.Level Convert(LogLevel level)
        {
            return PortableToLog4NetMap[level];
        }

        /// <summary>
        /// Converts a log4net level to a PortableLogAdapter level
        /// </summary>
        /// <param name="level">log4net level to convert</param>
        /// <returns>Equivalent PortableLogAdapter level</returns>
        public static LogLevel Convert(log4net.Core.Level level)
        {
            var result = LogLevel.Off;

            if (Log4NetToPortableMap.TryGetValue(level, out result) == false)
            {
                result = GetNearestLogLevel(level);
            }

            return result;
        }

        private static LogLevel GetNearestLogLevel(log4net.Core.Level level)
        {
            var result = LogLevel.Off;

            foreach (var l in Log4NetToPortableMap.Keys)
            {
                if (level.Value <= l.Value)
                {
                    result = Log4NetToPortableMap[l];

                    break;
                }
            }

            return result;
        }

        private static log4net.Core.Level GetLog4NetLevel(log4net.ILog logger)
        {
            return Log4NetToPortableMap.Keys.First(x => logger.Logger.IsEnabledFor(x));
        }
    }
}
