﻿using log4net;
using System;
using System.Collections.Generic;

namespace Acrotech.PortableLogAdapter.log4netAdapter
{
    /// <summary>
    /// This class creates log4net loggers
    /// </summary>
    public class Log4NetManager : ILogManager
    {
        /// <summary>
        /// This is the default (and only) Log4NetManager instance
        /// </summary>
        public static readonly Log4NetManager Default = new Log4NetManager();

        private Log4NetManager()
        {
        }

        /// <summary>
        /// Creates a new Log4NetLogger with the provided <paramref name="name"/>
        /// </summary>
        /// <param name="name">Logger name</param>
        /// <returns>The log4net logger</returns>
        public ILogger GetLogger(string name)
        {
            return new Log4NetLogger(log4net.LogManager.GetLogger(name));
        }
    }
}
