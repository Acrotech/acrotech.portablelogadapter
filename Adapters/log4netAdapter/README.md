﻿# README #

This library is intended to provide log4net support for platform targets that use the [PortableLogAdapter framework](https://bitbucket.org/Acrotech/acrotech.portablelogadapter).

## Usage Notes ##

*None Yet*

## Changelog ##

#### 1.0.0 ####

* Initial Release
