﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class ExtensionMethodsTest
    {
        [TestMethod]
        public void FormatWithTest()
        {
            string format = null;

            Assert.IsNull(format.FormatWith(), "NULL format, NO args");
            Assert.IsNull(format.FormatWith(null), "NULL format, NULL args");

            format = "test";

            Assert.AreEqual("test", format.FormatWith(), "static text, NO args");
            Assert.AreEqual("test", format.FormatWith(null), "static text, NULL args");
            Assert.AreEqual("test", format.FormatWith("p1"), "static text, single arg");

            format = "{0}";

            Assert.AreEqual(format, format.FormatWith(), "single replacement text, NO args");
            Assert.AreEqual(format, format.FormatWith(null), "single replacement text, NULL args");
            Assert.AreEqual("p1", format.FormatWith("p1"), "single replacement text, single arg");
            Assert.AreEqual("p1", format.FormatWith("p1", "p2"), "single replacement text, dual args");

            format = "{0}{1}";

            Assert.AreEqual(format, format.FormatWith(), "dual replacement text, NO args");
            Assert.AreEqual(format, format.FormatWith(null), "dual replacement text, NULL args");
            Assert.AreEqual("p1p2", format.FormatWith("p1", "p2"), "dual replacement text, dual args");
            Assert.AreEqual("p1p2", format.FormatWith("p1", "p2", "p3"), "dual replacement text, triple args");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        [ExcludeFromCodeCoverage]
        public void FormatWithBadFormatTest()
        {
            "{0}{1}".FormatWith("test");
        }
    }
}
