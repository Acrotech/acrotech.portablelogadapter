﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableLogAdapter.Test
{
    public class UnitTestLogManager : ILogManager
    {
        public static readonly UnitTestLogManager Default = new UnitTestLogManager();

        #region ILogManager Members

        public ILogger GetLogger(string name)
        {
            return new UnitTestLogger(name);
        }

        #endregion
    }

    public class UnitTestLogger : ILogger
    {
        public UnitTestLogger(string name)
        {
            Name = name;
        }

        public LogLevel LastLogLevel { get; private set; }

        public string LastMessage { get; private set; }

        public Exception LastException { get; private set; }

        private void Record(LogLevel level, string message, Exception exception = null)
        {
            LastLogLevel = level;
            LastMessage = message;
            LastException = exception;
        }

        #region ILogger Members

        public string Name { get; private set; }

        public void Log(LogLevel level, string format, params object[] args)
        {
            Record(level, format.FormatWith(args));
        }

        public void Log(LogLevel level, Func<string> messageCreator)
        {
            Record(level, messageCreator());
        }

        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            Record(level, format.FormatWith(args), exception);
        }

        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            Record(level, messageCreator(), exception);
        }

        public LogLevel LogLevel { get; set; }

        #endregion
    }
}
