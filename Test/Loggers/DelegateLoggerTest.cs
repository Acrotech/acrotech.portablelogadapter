﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Test.Loggers
{
    [TestClass]
    public class DelegateLoggerTest
    {
        private static bool WasDelegateInvoked = false;

        private static Action<ILogger, string> LoggerDelegate = (_, __) => WasDelegateInvoked = true;

        [TestMethod]
        public void ConstructorTest()
        {
            Action<ILogger, string> delegateAction = (_, __) => { };
            
            var logger = new DelegateLogger("test", LogLevel.Info, delegateAction);
            var po = new PrivateObject(logger);

            Assert.AreEqual("test", logger.Name);
            Assert.AreEqual(LogLevel.Info, logger.LogLevel);
            Assert.AreSame(delegateAction, po.GetProperty("DelegateAction"));

            // ensure no exceptions occur
            logger.Info("test");
        }

        [TestMethod]
        public void LogTest()
        {
            var logger = new DelegateLogger(null, LogLevel.Trace, null);

            // ensure no exceptions occur
            logger.Trace("test");

            logger = new DelegateLogger(null, LogLevel.Trace, LoggerDelegate);

            WasDelegateInvoked = false;
            logger.Trace("test");
            Assert.IsTrue(WasDelegateInvoked);

            WasDelegateInvoked = false;
            logger.Trace(() => "test");
            Assert.IsTrue(WasDelegateInvoked);

            logger.LogLevel = LogLevel.Info;
            
            WasDelegateInvoked = false;
            logger.Trace("test");
            Assert.IsFalse(WasDelegateInvoked);

            WasDelegateInvoked = false;
            logger.Trace(() => "test");
            Assert.IsFalse(WasDelegateInvoked);
        }

        [TestMethod]
        public void LogFormattingTest()
        {
            string message = null;
            Action<ILogger, string> delegateAction = (_, x) => message = x;

            var logger = new DelegateLogger(null, LogLevel.Trace, delegateAction);

            message = null;
            logger.Trace("test");
            Assert.AreEqual("[Trace] test", message);

            message = null;
            logger.Debug("test");
            Assert.AreEqual("[Debug] test", message);

            message = null;
            logger.Info("test");
            Assert.AreEqual("[Info ] test", message);

            message = null;
            logger.Warn("test");
            Assert.AreEqual("[Warn ] test", message);

            message = null;
            logger.Error("test");
            Assert.AreEqual("[Error] test", message);

            message = null;
            logger.Fatal("test");
            Assert.AreEqual("[Fatal] test", message);
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            var logger = new DelegateLogger(null, LogLevel.Trace, null);

            // ensure no exceptions occur
            logger.TraceException(new Exception(), "test");

            logger = new DelegateLogger(null, LogLevel.Trace, LoggerDelegate);

            WasDelegateInvoked = false;
            logger.TraceException(new Exception(), "test");
            Assert.IsTrue(WasDelegateInvoked);

            WasDelegateInvoked = false;
            logger.TraceException(new Exception(), () => "test");
            Assert.IsTrue(WasDelegateInvoked);

            logger.LogLevel = LogLevel.Info;

            WasDelegateInvoked = false;
            logger.TraceException(new Exception(), "test");
            Assert.IsFalse(WasDelegateInvoked);

            WasDelegateInvoked = false;
            logger.TraceException(new Exception(), () => "test");
            Assert.IsFalse(WasDelegateInvoked);
        }

        [TestMethod]
        public void LogExceptionFormattingTest()
        {
            string message = null;
            Action<ILogger, string> delegateAction = (_, x) => message = x;

            var logger = new DelegateLogger(null, LogLevel.Trace, delegateAction);

            var exception = new Exception("testing");

            message = null;
            logger.TraceException(exception, "test");
            Assert.AreEqual("[Trace] test: System.Exception: testing", message);

            message = null;
            logger.DebugException(exception, "test");
            Assert.AreEqual("[Debug] test: System.Exception: testing", message);

            message = null;
            logger.InfoException(exception, "test");
            Assert.AreEqual("[Info ] test: System.Exception: testing", message);

            message = null;
            logger.WarnException(exception, "test");
            Assert.AreEqual("[Warn ] test: System.Exception: testing", message);

            message = null;
            logger.ErrorException(exception, "test");
            Assert.AreEqual("[Error] test: System.Exception: testing", message);

            message = null;
            logger.FatalException(exception, "test");
            Assert.AreEqual("[Fatal] test: System.Exception: testing", message);
        }
    }
}
