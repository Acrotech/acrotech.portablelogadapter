﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter.Loggers;
using System.Diagnostics.CodeAnalysis;

namespace Acrotech.PortableLogAdapter.Test.Loggers
{
    [TestClass]
    public class NullLoggerTest
    {
        [TestMethod]
        public void DefaultLoggerTest()
        {
            Assert.IsNotNull(NullLogger.Default);
        }

        [TestMethod]
        public void LoggerNameTest()
        {
            Assert.AreEqual(NullLogger.LoggerName, NullLogger.Default.Name);
        }

        [TestMethod]
        public void LogTest()
        {
            // ensure no exceptions occur
            NullLogger.Default.Log(LogLevel.Trace, (string)null);
            NullLogger.Default.Log(LogLevel.Trace, (Func<string>)null);
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            // ensure no exceptions occur
            NullLogger.Default.LogException(LogLevel.Trace, new Exception(), (string)null);
            NullLogger.Default.LogException(LogLevel.Trace, new Exception(), (Func<string>)null);
        }

        [TestMethod]
        public void LogLevelTest()
        {
            Assert.AreEqual(LogLevel.Off, NullLogger.Default.LogLevel);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void LogLevelSetterTest()
        {
            NullLogger.Default.LogLevel = LogLevel.Trace;
        }
    }
}
