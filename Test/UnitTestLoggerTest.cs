﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter.Test.Contracts;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class UnitTestLoggerTest
    {
        [TestMethod]
        public void NameTest()
        {
            var logger = new UnitTestLogger(null);
            Assert.AreEqual(null, logger.Name);

            logger = new UnitTestLogger(string.Empty);
            Assert.AreEqual(string.Empty, logger.Name);

            logger = new UnitTestLogger("test");
            Assert.AreEqual("test", logger.Name);
        }

        [TestMethod]
        public void RecordTest()
        {
            var logger = new UnitTestLogger(null);
            var po = new PrivateObject(logger);

            po.Invoke("Record", LogLevel.Info, "test1", null);
            Assert.AreEqual(LogLevel.Info, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.AreEqual(null, logger.LastException);

            var exception = new Exception();
            po.Invoke("Record", LogLevel.Warn, "test2", exception);
            Assert.AreEqual(LogLevel.Warn, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);
        }

        [TestMethod]
        public void LogTest()
        {
            var logger = new UnitTestLogger(null);

            logger.Log(LogLevel.Info, "test1");
            Assert.AreEqual(LogLevel.Info, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            logger.Log(LogLevel.Warn, () => "test2");
            Assert.AreEqual(LogLevel.Warn, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.IsNull(logger.LastException);
        }

        [TestMethod]
        public void LogExceptionTest()
        {
            var logger = new UnitTestLogger(null);

            var exception = new Exception();
            logger.LogException(LogLevel.Info, exception, "test1");
            Assert.AreEqual(LogLevel.Info, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            exception = new Exception();
            logger.LogException(LogLevel.Warn, exception, () => "test2");
            Assert.AreEqual(LogLevel.Warn, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);
        }

        [TestMethod]
        public void IsEnabledExtensionTest()
        {
            ILogger logger = new UnitTestLogger(null);

            logger.LogLevel = LogLevel.Trace;

            foreach (var level in LogLevelExtensionsTest.AllLevels)
            {
                Assert.IsTrue(logger.IsEnabled(level), "UnitTestLogger should be enabled for all levels ({0} is disabled)", level);
            }

            logger.LogLevel = LogLevel.Off;

            foreach (var level in LogLevelExtensionsTest.AllLevels.Where(x => x != LogLevel.Off))
            {
                Assert.IsFalse(logger.IsEnabled(level), "UnitTestLogger should be disabled for all levels ({0} is enabled)", level);
            }

            Assert.IsTrue(logger.IsEnabled(LogLevel.Off), "UnitTestLogger should be enabled for LogLevel.Off");
        }

        [TestMethod]
        public void TraceExtensionsTest()
        {
            var logger = new UnitTestLogger(null);

            logger.Trace("test1");
            Assert.AreEqual(LogLevel.Trace, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            logger.Trace(() => "test2");
            Assert.AreEqual(LogLevel.Trace, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            var exception = new Exception();
            logger.TraceException(exception, "test3");
            Assert.AreEqual(LogLevel.Trace, logger.LastLogLevel);
            Assert.AreEqual("test3", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            exception = new Exception();
            logger.TraceException(exception, () => "test4");
            Assert.AreEqual(LogLevel.Trace, logger.LastLogLevel);
            Assert.AreEqual("test4", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            Assert.IsTrue(logger.IsTraceEnabled());
            logger.LogLevel = LogLevel.Debug;
            Assert.IsFalse(logger.IsTraceEnabled());
        }

        [TestMethod]
        public void DebugExtensionsTest()
        {
            var logger = new UnitTestLogger(null);

            logger.Debug("test1");
            Assert.AreEqual(LogLevel.Debug, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            logger.Debug(() => "test2");
            Assert.AreEqual(LogLevel.Debug, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            var exception = new Exception();
            logger.DebugException(exception, "test3");
            Assert.AreEqual(LogLevel.Debug, logger.LastLogLevel);
            Assert.AreEqual("test3", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            exception = new Exception();
            logger.DebugException(exception, () => "test4");
            Assert.AreEqual(LogLevel.Debug, logger.LastLogLevel);
            Assert.AreEqual("test4", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            Assert.IsTrue(logger.IsDebugEnabled());
            logger.LogLevel = LogLevel.Info;
            Assert.IsFalse(logger.IsDebugEnabled());
        }

        [TestMethod]
        public void InfoExtensionsTest()
        {
            var logger = new UnitTestLogger(null);

            logger.Info("test1");
            Assert.AreEqual(LogLevel.Info, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            logger.Info(() => "test2");
            Assert.AreEqual(LogLevel.Info, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            var exception = new Exception();
            logger.InfoException(exception, "test3");
            Assert.AreEqual(LogLevel.Info, logger.LastLogLevel);
            Assert.AreEqual("test3", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            exception = new Exception();
            logger.InfoException(exception, () => "test4");
            Assert.AreEqual(LogLevel.Info, logger.LastLogLevel);
            Assert.AreEqual("test4", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            Assert.IsTrue(logger.IsInfoEnabled());
            logger.LogLevel = LogLevel.Warn;
            Assert.IsFalse(logger.IsInfoEnabled());
        }

        [TestMethod]
        public void WarnExtensionsTest()
        {
            var logger = new UnitTestLogger(null);

            logger.Warn("test1");
            Assert.AreEqual(LogLevel.Warn, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            logger.Warn(() => "test2");
            Assert.AreEqual(LogLevel.Warn, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            var exception = new Exception();
            logger.WarnException(exception, "test3");
            Assert.AreEqual(LogLevel.Warn, logger.LastLogLevel);
            Assert.AreEqual("test3", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            exception = new Exception();
            logger.WarnException(exception, () => "test4");
            Assert.AreEqual(LogLevel.Warn, logger.LastLogLevel);
            Assert.AreEqual("test4", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            Assert.IsTrue(logger.IsWarnEnabled());
            logger.LogLevel = LogLevel.Error;
            Assert.IsFalse(logger.IsWarnEnabled());
        }

        [TestMethod]
        public void ErrorExtensionsTest()
        {
            var logger = new UnitTestLogger(null);

            logger.Error("test1");
            Assert.AreEqual(LogLevel.Error, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            logger.Error(() => "test2");
            Assert.AreEqual(LogLevel.Error, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            var exception = new Exception();
            logger.ErrorException(exception, "test3");
            Assert.AreEqual(LogLevel.Error, logger.LastLogLevel);
            Assert.AreEqual("test3", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            exception = new Exception();
            logger.ErrorException(exception, () => "test4");
            Assert.AreEqual(LogLevel.Error, logger.LastLogLevel);
            Assert.AreEqual("test4", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            Assert.IsTrue(logger.IsErrorEnabled());
            logger.LogLevel = LogLevel.Fatal;
            Assert.IsFalse(logger.IsErrorEnabled());
        }

        [TestMethod]
        public void FatalExtensionsTest()
        {
            var logger = new UnitTestLogger(null);

            logger.Fatal("test1");
            Assert.AreEqual(LogLevel.Fatal, logger.LastLogLevel);
            Assert.AreEqual("test1", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            logger.Fatal(() => "test2");
            Assert.AreEqual(LogLevel.Fatal, logger.LastLogLevel);
            Assert.AreEqual("test2", logger.LastMessage);
            Assert.IsNull(logger.LastException);

            var exception = new Exception();
            logger.FatalException(exception, "test3");
            Assert.AreEqual(LogLevel.Fatal, logger.LastLogLevel);
            Assert.AreEqual("test3", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            exception = new Exception();
            logger.FatalException(exception, () => "test4");
            Assert.AreEqual(LogLevel.Fatal, logger.LastLogLevel);
            Assert.AreEqual("test4", logger.LastMessage);
            Assert.AreEqual(exception, logger.LastException);

            Assert.IsTrue(logger.IsFatalEnabled());
            logger.LogLevel = LogLevel.Off;
            Assert.IsFalse(logger.IsFatalEnabled());
        }
    }
}
