﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableLogAdapter.Test
{
    [TestClass]
    public class UnitTestLogManagerTest
    {
        [TestMethod]
        public void DefaultManagerTest()
        {
            Assert.IsNotNull(UnitTestLogManager.Default);
        }

        [TestMethod]
        public void GetLoggerTest()
        {
            var manager = UnitTestLogManager.Default;
            ILogger logger = null;

            logger = manager.GetLogger((string)null);
            Assert.IsNotNull(logger);
            Assert.IsNull(logger.Name);

            logger = manager.GetLogger(string.Empty);
            Assert.IsNotNull(logger);
            Assert.AreEqual(string.Empty, logger.Name);

            logger = manager.GetLogger("test");
            Assert.IsNotNull(logger);
            Assert.AreEqual("test", logger.Name);
        }

        [TestMethod]
        public void GetLoggerExtensionsTest()
        {
            var manager = UnitTestLogManager.Default;
            ILogger logger = null;

            logger = manager.GetLogger();
            Assert.IsNotNull(logger);
            Assert.AreEqual(string.Empty, logger.Name);

            logger = manager.GetLogger(this);
            Assert.IsNotNull(logger);
            Assert.AreEqual(GetType().GetLoggerName(), logger.Name);

            logger = manager.GetLogger(GetType());
            Assert.IsNotNull(logger);
            Assert.AreEqual(GetType().GetLoggerName(), logger.Name);

            logger = manager.GetLogger<UnitTestLogManagerTest>();
            Assert.IsNotNull(logger);
            Assert.AreEqual(typeof(UnitTestLogManagerTest).GetLoggerName(), logger.Name);
        }
    }
}
