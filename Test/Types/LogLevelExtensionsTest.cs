﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableLogAdapter.Test.Types
{
    [TestClass]
    public class LogLevelExtensionsTest
    {
        [TestMethod]
        public void GetAllLogLevelsTest()
        {
            var expected = Contracts.LogLevelExtensionsTest.AllLevels.Where(x => x != LogLevel.Off).OrderBy(x => x);
            var actual = ExtensionMethods.GetAllLogLevels().OrderBy(x => x);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [TestMethod]
        public void IsEnabledForTest()
        {
            foreach (var level in Contracts.LogLevelExtensionsTest.AllLevels)
            {
                foreach (var comparisonLevel in Contracts.LogLevelExtensionsTest.AllLevels)
                {
                    if (level >= comparisonLevel)
                    {
                        Assert.IsTrue(level.IsEnabledFor(comparisonLevel));
                    }
                    else
                    {
                        Assert.IsFalse(level.IsEnabledFor(comparisonLevel));
                    }
                }
            }
        }
    }
}
