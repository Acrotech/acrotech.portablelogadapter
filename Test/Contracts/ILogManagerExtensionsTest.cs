﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableLogAdapter.Test.Contracts
{
    [TestClass]
    public class ILogManagerExtensionsTest
    {
        [TestMethod]
        public void GetLoggerNameExtensionTest()
        {
            Type type = null;

            Assert.AreEqual(string.Empty, type.GetLoggerName());
            Assert.AreEqual(GetType().Name, GetType().GetLoggerName());
        }

        [TestMethod]
        public void GetLoggerExtensionsTest()
        {
            ILogManager manager = null;

            Assert.IsNull(manager.GetLogger());
            Assert.IsNull(manager.GetLogger(this));
            Assert.IsNull(manager.GetLogger(GetType()));
            Assert.IsNull(manager.GetLogger<ILogManagerExtensionsTest>());
        }

        [TestMethod]
        public void GetAllLogLevelsExtensionTest()
        {
            ILogManager manager = null;

            var expected = LogLevelExtensionsTest.AllLevels.Where(x => x != LogLevel.Off).OrderBy(x => x);
            var actual = manager.GetAllLogLevels().OrderBy(x => x).ToArray();

            Assert.IsTrue(expected.SequenceEqual(actual), "NULL LogManager");
        }
    }
}
