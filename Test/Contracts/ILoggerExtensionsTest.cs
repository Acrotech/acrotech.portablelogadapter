﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableLogAdapter.Test.Contracts
{
    [TestClass]
    public class ILoggerExtensionsTest
    {
        [TestMethod]
        public void GetAllLogLevelsExtensionTest()
        {
            ILogger logger = null;

            var expected = LogLevelExtensionsTest.AllLevels.Where(x => x != LogLevel.Off).OrderBy(x => x);
            var actual = logger.GetAllLogLevels().OrderBy(x => x);

            Assert.IsTrue(expected.SequenceEqual(actual), "NULL Logger");
        }

        [TestMethod]
        public void IsEnabledExtensionTest()
        {
            ILogger logger = null;

            foreach (var level in LogLevelExtensionsTest.AllLevels)
            {
                Assert.IsFalse(logger.IsEnabled(level), "NULL Logger should be disabled for all levels ({0} is enabled)", level);
            }
        }
    }
}
