﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableLogAdapter.Test.Contracts
{
    [TestClass]
    public class LogLevelExtensionsTest
    {
        public static readonly LogLevel[] AllLevels = (LogLevel[])Enum.GetValues(typeof(LogLevel));

        private static bool WasMessageCreatorInvoked = false;

        private static Func<string> MessageCreatorDelegate = () => { WasMessageCreatorInvoked = true; return string.Empty; };

        [TestMethod]
        public void MessageCreatorTest()
        {
            WasMessageCreatorInvoked = false;

            MessageCreatorDelegate();
            Assert.IsTrue(WasMessageCreatorInvoked);
        }

        [TestMethod]
        public void TraceExtensionsTest()
        {
            ILogger logger = null;

            // Ensure no exceptions occur and that the delegate is not invoked
            WasMessageCreatorInvoked = false;
            logger.Trace("test");
            logger.Trace(MessageCreatorDelegate);
            logger.TraceException(new Exception(), "test");
            logger.TraceException(new Exception(), MessageCreatorDelegate);
            Assert.IsFalse(WasMessageCreatorInvoked);
            Assert.IsFalse(logger.IsTraceEnabled());
        }

        [TestMethod]
        public void DebugExtensionsTest()
        {
            ILogger logger = null;

            // Ensure no exceptions occur and that the delegate is not invoked
            WasMessageCreatorInvoked = false;
            logger.Debug("test");
            logger.Debug(MessageCreatorDelegate);
            logger.DebugException(new Exception(), "test");
            logger.DebugException(new Exception(), MessageCreatorDelegate);
            Assert.IsFalse(WasMessageCreatorInvoked);
            Assert.IsFalse(logger.IsDebugEnabled());
        }

        [TestMethod]
        public void InfoExtensionsTest()
        {
            ILogger logger = null;

            // Ensure no exceptions occur and that the delegate is not invoked
            WasMessageCreatorInvoked = false;
            logger.Info("test");
            logger.Info(MessageCreatorDelegate);
            logger.InfoException(new Exception(), "test");
            logger.InfoException(new Exception(), MessageCreatorDelegate);
            Assert.IsFalse(WasMessageCreatorInvoked);
            Assert.IsFalse(logger.IsInfoEnabled());
        }

        [TestMethod]
        public void WarnExtensionsTest()
        {
            ILogger logger = null;

            // Ensure no exceptions occur and that the delegate is not invoked
            WasMessageCreatorInvoked = false;
            logger.Warn("test");
            logger.Warn(MessageCreatorDelegate);
            logger.WarnException(new Exception(), "test");
            logger.WarnException(new Exception(), MessageCreatorDelegate);
            Assert.IsFalse(WasMessageCreatorInvoked);
            Assert.IsFalse(logger.IsWarnEnabled());
        }

        [TestMethod]
        public void ErrorExtensionsTest()
        {
            ILogger logger = null;

            // Ensure no exceptions occur and that the delegate is not invoked
            WasMessageCreatorInvoked = false;
            logger.Error("test");
            logger.Error(MessageCreatorDelegate);
            logger.ErrorException(new Exception(), "test");
            logger.ErrorException(new Exception(), MessageCreatorDelegate);
            Assert.IsFalse(WasMessageCreatorInvoked);
            Assert.IsFalse(logger.IsErrorEnabled());
        }

        [TestMethod]
        public void FatalExtensionsTest()
        {
            ILogger logger = null;

            // Ensure no exceptions occur and that the delegate is not invoked
            WasMessageCreatorInvoked = false;
            logger.Fatal("test");
            logger.Fatal(MessageCreatorDelegate);
            logger.FatalException(new Exception(), "test");
            logger.FatalException(new Exception(), MessageCreatorDelegate);
            Assert.IsFalse(WasMessageCreatorInvoked);
            Assert.IsFalse(logger.IsFatalEnabled());
        }
    }
}
