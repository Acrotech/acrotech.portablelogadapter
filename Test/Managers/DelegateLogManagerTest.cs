﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter.Managers;
using Acrotech.PortableLogAdapter.Loggers;
using System.Diagnostics.CodeAnalysis;

namespace Acrotech.PortableLogAdapter.Test.Managers
{
    [TestClass]
    public class DelegateLogManagerTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Action<ILogger, string> logAction = (_, __) => { };

            var manager = new DelegateLogManager(logAction);
            var po = new PrivateObject(manager);

            Assert.AreSame(logAction, po.GetProperty("DelegateAction"));
            Assert.AreEqual(LogLevel.Trace, manager.DefaultLevel);

            manager = new DelegateLogManager(logAction, LogLevel.Info);
            po = new PrivateObject(manager);

            Assert.AreSame(logAction, po.GetProperty("DelegateAction"));
            Assert.AreEqual(LogLevel.Info, manager.DefaultLevel);

            // ensure no exceptions occur
            var logger = manager.GetLogger();
            logger.Info(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void ConstructorWithNullDelegateTest()
        {
            new DelegateLogManager(null);
        }

        [TestMethod]
        public void GetLoggerTest()
        {
            Action<ILogger, string> logAction = (_, __) => { };

            var manager = new DelegateLogManager(logAction);
            var logger = manager.GetLogger("test");
            var po = new PrivateObject(logger);

            Assert.IsNotNull(logger);
            Assert.AreEqual("test", logger.Name);
            Assert.AreSame(logAction, po.GetProperty("DelegateAction"));
            Assert.AreEqual(manager.DefaultLevel, logger.LogLevel);

            manager.DefaultLevel = LogLevel.Info;
            logger = manager.GetLogger("test");
            Assert.AreEqual(LogLevel.Info, logger.LogLevel);

            // ensure a null or empty name is allowed
            logger = manager.GetLogger(null);
            Assert.IsNotNull(logger);
            Assert.IsNull(logger.Name);

            logger = manager.GetLogger(string.Empty);
            Assert.IsNotNull(logger);
            Assert.AreEqual(string.Empty, logger.Name);

            // ensure no exceptions occur
            logger = manager.GetLogger();
            logger.Info(string.Empty);
        }
    }
}
