﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableLogAdapter.Managers;
using Acrotech.PortableLogAdapter.Loggers;

namespace Acrotech.PortableLogAdapter.Test.Managers
{
    [TestClass]
    public class NullLogManagerTest
    {
        [TestMethod]
        public void DefaultManagerTest()
        {
            Assert.IsNotNull(NullLogManager.Default);
        }

        [TestMethod]
        public void GetLoggerTest()
        {
            var manager = NullLogManager.Default;
            ILogger logger = null;

            logger = manager.GetLogger((string)null);
            Assert.IsNotNull(logger);
            Assert.AreEqual(NullLogger.Default.Name, logger.Name);

            logger = manager.GetLogger(string.Empty);
            Assert.IsNotNull(logger);
            Assert.AreEqual(NullLogger.Default.Name, logger.Name);

            logger = manager.GetLogger("test");
            Assert.IsNotNull(logger);
            Assert.AreEqual(NullLogger.Default.Name, logger.Name);
        }
    }
}
