# This template build customization file provides a list of all standard build
# function signatures and includes an example calling the function. For more
# details, please see the README.
#
# function Prep-Project(
#   [string]$basePath,
#   [string]$buildPath = $env:DefaultBuildPropertiesPath,
#   [string]$versionPath = $env:DefaultVersionPropertiesPath
# )
#
# prepare the project by writing to the properties files. Allows overriding the
# default locations of the properties files.
#
# Example:
# Prep-Project (Get-Path "My.Project")
#
# ------------------------------------------------------------------------------
#
# function Build(
#   [string]$basePath,
#   [string]$projectFileName,
#   [string]$packagesFileName = $env:DefaultPackagesFile,
#   [string]$restoreArgs = $env:DefaultNuGetRestoreArgs,
#   [string]$versionDetailsPath = $env:DefaultVersionDetailsPath,
#   [string]$transformArgs = $env:DefaultTransformArgs,
#   [string]$buildArgs = $env:DefaultBuildArgs
# )
#
# composite function that calls Restore-Packages, Transform-Project-Version, and
# Build-Project functions with corresponding parameters. $basePath is the path
# to the project's directory.
#
# Example:
# Build (Get-Path "My.Project") "My.Project.csproj"
#
# ------------------------------------------------------------------------------
#
# function Restore-Packages(
#   [string]$packagesPath,
#   [string]$solutionDirectory = $env:SourcesPath,
#   [string]$restoreArgs = $env:DefaultNuGetRestoreArgs
# )
#
# performs a NuGet restore operation on the provided packages.config path.
# Allows overriding the solution directory and the NuGet restore args.
#
# Example:
# Restore-Packages (Get-Path "My.Project\packages.config")
#
# ------------------------------------------------------------------------------
#
# function Transform-Project-Version(
#   [string]$basePath,
#   [string]$versionDetailsPath = $env:DefaultVersionDetailsPath,
#   [string]$transformArgs = $env:DefaultTransformArgs
# )
#
# Transforms the version details file. $basePath is the path to the project's
# directory. Allows overriding the path to the version details file and the args
# the TextTransform.exe is called with.
#
# Example:
# Transform-Project-Version (Get-Path "My.Project")
#
# ------------------------------------------------------------------------------
#
# function Build-Project(
#   [string]$projectPath,
#   [string]$buildArgs = $env:DefaultBuildArgs
# )
#
# Runs MSBuild on the provided project path. Allows overriding the build args
# passed to MSBuild.
#
# Example:
# Build-Project (Get-Path "My.Project\My.Project.csproj")
#
# ------------------------------------------------------------------------------
#
# function Run-Tests(
#   [string]$assemblyPath,
#   [string]$settingsPath = $null,
#   [string]$testArgs = $env:DefaultTestArgs
# )
#
# Runs the tests found in the provided assembly path. Allows providing a
# *.runsettings path and overriding the args passed to VsTest.Console.exe.
#
# Example:
# Run-Tests (Get-Path "My.Project.Test\bin\$env:Configuration\My.Project.Test.dll")
#
# ------------------------------------------------------------------------------
#
# function Pack-Packagable(
#   [string]$packagablePath,
#   [string]$packArgs = $env:DefaultNuGetPackArgs
# )
#
# performs a NuGet pack operation on the provided packagable path. Allows
# overriding the NuGet pack args. NOTE: a packagable can be either a *.nuspec
# file or a project file (i.e., *.csproj).
#
# Example:
# Pack-Packagable (Get-Path "My.Project\My.Project.csproj")
#
# ------------------------------------------------------------------------------
