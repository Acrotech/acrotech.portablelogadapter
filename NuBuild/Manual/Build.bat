@ECHO OFF

REM if you want to just run powershell scripts normally, then you must run the following command in an elevated powershell terminal
REM Set-ExecutionPolicy RemoteSigned
CALL powershell -ExecutionPolicy ByPass -File "%~dp0.\Build.ps1"

PAUSE
